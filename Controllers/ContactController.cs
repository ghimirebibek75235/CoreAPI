﻿using CoreAPI.Data;
using CoreAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CoreAPI.Controllers
{
    [ApiController]
    [Route("api/Contact")]
    public class ContactController : Controller
    {
        private readonly ContactAPIDbContext db_Context;

        public ContactController(ContactAPIDbContext dbContext)
        {
            this.db_Context = dbContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllContacts()
        {
            return Ok(await db_Context.Contacts.ToListAsync());
        }


        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetContactById([FromRoute] Guid id) /*Using [FromRoute] is optional here*/
        {
            var contact = await db_Context.Contacts.FindAsync(id);
            if (contact == null)
            {
                return NotFound();
            }
            return Ok(contact);

        }

        [HttpPost]
        public async Task<IActionResult> AddContact(AddContact ac)
        {
            var contact = new Contact()
            {
                Id = Guid.NewGuid(),
                Address = ac.Address,
                Email = ac.Email,
                Phone = ac.Phone,
                FullName = ac.FullName
            };
            await db_Context.Contacts.AddAsync(contact);
            await db_Context.SaveChangesAsync();
            return Ok(contact);
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateContact([FromRoute] Guid id, UpdateContact uc)
        {
            var contactCheck =  await db_Context.Contacts.FindAsync(id);
            if (contactCheck != null)
            {
                contactCheck.Email = uc.Email;
                contactCheck.Phone = uc.Phone;
                contactCheck.FullName = uc.FullName;
                contactCheck.Address = uc.Address;

                await db_Context.SaveChangesAsync();
                return Ok(contactCheck);
            }
            return NotFound();
            
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteContact([FromRoute] Guid id)
        {
            var contact = await db_Context.Contacts.FindAsync(id);
            if (contact != null)
            {
                db_Context.Contacts.Remove(contact);
                await db_Context.SaveChangesAsync();
                return Ok(contact);
            }
            return NotFound();
        }
    }
}
