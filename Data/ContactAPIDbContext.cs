﻿using CoreAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreAPI.Data
{
    public class ContactAPIDbContext : DbContext
    {
        public ContactAPIDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }
    }
}
